$(document).ready(function(){
  $('#open-menu').click(function(){
    $('#menu-mobile').addClass('open');
    $('#bg-header').addClass('bg-header');
    $('#menu-mobile').stop().animate({ 
      left: "0%"
    }, 500);   
    event.stopPropagation(); 
  });
  $('#close-sidebar').click(function(){
    $('#menu-mobile').removeClass('open');
    $('#bg-header').removeClass('bg-header');
    $('#menu-mobile').stop().css('left', '0').animate({
      left: '-=100%'
    }, 500);
    event.stopPropagation(); 
  });
  $('#bg-header').not("#menu-mobile").click(function(){
    if ($('#menu-mobile').hasClass('open') && !$(event.target).is('#menu-mobile')) {
      $('#menu-mobile').removeClass('open');
      $('#bg-header').removeClass('bg-header');
      $('#menu-mobile').stop().css('left', '0').animate({
        left: '-=100%'
      }, 500);
      event.stopPropagation(); 
    }
  })





  $('.owl-carousel-basic').owlCarousel({
    loop:true,      
    // autoplay:true,  
    item: 4,
    // autoplayTimeout:3000,    
    autoplayHoverPause:true,  
    nav:true,   
    navText: ["<img src='./images/prev.png'>","<img src='./images/next.png'>"],    
    responsive:{  
      0:{
        items:1
      },
      600:{
        items:2,
        nav:false,
        margin: 50
      },
      1000:{
        items:3,
        margin: 40
      }
    }
  })
});